package com.zhoyq.service.demo.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author 刘路
 * @email feedback@zhoyq.com
 * @date 2018/7/21
 */
@Service
public class DemoService {

    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "helloFallback")
    public String hello() {
         return restTemplate.getForEntity("http://DEMO-SERVICE/world",String.class).getBody();
    }

    public String helloFallback(){
        return "error";
    }
}
