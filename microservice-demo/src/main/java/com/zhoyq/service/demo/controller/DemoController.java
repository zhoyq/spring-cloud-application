package com.zhoyq.service.demo.controller;

import com.zhoyq.service.demo.service.DemoService;
import com.zhoyq.service.demo.service.HelloService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author 刘路
 * @email feedback@zhoyq.com
 * @date 2018/7/21
 */
@RestController
public class DemoController {

    private final Logger log = LoggerFactory.getLogger(DemoController.class);

    @Value("${book.name}")
    private String bookName;
    @Value("${book.author}")
    private String bookAuthor;
    @Value("${book.desc}")
    private String bookDesc;

    @Autowired
    private DiscoveryClient client;
    @Autowired
    private DemoService demoService;
    @Autowired
    private HelloService helloService;

    @RequestMapping("/hello2")
    public String hello2() {
        ServiceInstance instance = client.getLocalServiceInstance();
        log.info("/hello, host:" + instance.getHost() + ", service_id:" + instance.getServiceId());
        String body = helloService.world();
        return "Hello " + bookName + ":" + bookAuthor + "\n" + bookDesc + "<br>" + body;
    }

    @RequestMapping("/hello")
    public String hello() {
        ServiceInstance instance = client.getLocalServiceInstance();
        log.info("/hello, host:" + instance.getHost() + ", service_id:" + instance.getServiceId());
        String body = demoService.hello();
        return "Hello " + bookName + ":" + bookAuthor + "\n" + bookDesc + "<br>" + body;
    }

    @RequestMapping("/world")
    public String world(){
        return "World2";
    }
}
