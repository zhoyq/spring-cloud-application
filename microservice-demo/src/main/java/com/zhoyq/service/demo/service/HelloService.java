package com.zhoyq.service.demo.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author 刘路
 * @email feedback@zhoyq.com
 * @date 2018/8/4
 */
@FeignClient("demo-service")
public interface HelloService {
    @RequestMapping("/world")
    String world();
}
